public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads(String check){
       list<List<SObject>> searchstring = [FIND :check  IN ALL FIELDS RETURNING Lead(LastName), Contact(LastName)];
           return searchstring;  
    }
}