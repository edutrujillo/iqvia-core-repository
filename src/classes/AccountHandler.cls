public class AccountHandler {
 public static Account insertNewAccount(String name) {
     try {
        Account acct = new Account(Name=name);
        insert acct;
        ID ret = acct.Id;
        System.debug('ID = ' + ret);
        return acct;
    
     } catch (DmlException e) {
        System.debug('A DML exception has occurred: ' +
                    e.getMessage());
         return null;
     }  
}
}