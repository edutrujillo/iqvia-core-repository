public with sharing class callSSL {

    @AuraEnabled
    public static String getInfo(){ 
 		HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:callSSL/servicioB2CExp_EI3a/');
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getStatusCode());
		String ret = String.valueOf(res.getStatusCode());
		return ret;
    }
    @AuraEnabled
    public static String getGoogleInfo(){ 
 		HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:callGoogle');
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
		String ret = String.valueOf(res.getBody());
		return ret;
    }
   
}