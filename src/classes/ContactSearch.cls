public class ContactSearch {
     public static List<Contact> searchForContacts(String name, String pc) {
     try {
       	Contact[] contacts = [SELECT Id,Name,FirstName,LastName,MailingPostalCode
                          FROM Contact WHERE Name=:name or MailingPostalCode = :pc];
		return contacts;
    
     } catch (DmlException e) {
        System.debug('A DML exception has occurred: ' +
                    e.getMessage());
         return null;
     }  
}
}