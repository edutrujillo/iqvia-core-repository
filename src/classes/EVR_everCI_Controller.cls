public class EVR_everCI_Controller {
	@AuraEnabled
    public static String getTasksInfo(String name){
        String ret = '';
        // Instantiate a new http object
        Http h = new Http();
    	String url = 'http://ec2-52-51-178-250.eu-west-1.compute.amazonaws.com:28080/microeverci-rest/rest/infoServices/getTasks/';
         // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
    
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        ret = res.getBody();
		return ret;
    }
}