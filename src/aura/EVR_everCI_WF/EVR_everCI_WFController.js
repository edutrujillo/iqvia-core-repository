({
	doInit : function(component, event, helper) {
    $(document).ready(function() {
    var data = {
      operators: {
        operator1: {
          top: 20,
          left: 20,
          properties: {
            title: 'Operator 1',
            inputs: {},
            outputs: {
              output_1: {
                label: 'Output 1',
              }
            }
          }
        },
        operator2: {
          top: 80,
          left: 300,
          properties: {
            title: 'Operator 2',
            inputs: {
              input_1: {
                label: 'Input 1',
              },
              input_2: {
                label: 'Input 2',
              },
            },
            outputs: {}
          }
        },
      }
    };

    // Apply the plugin on a standard, empty div...
    $('#example').flowchart({
      data: data
    });
  });
  var operatorI = 0;
    $('#create_operator').click(function() {
      var operatorId = 'created_operator_' + operatorI;
      var operatorData = {
        top: 60,
        left: 500,
        properties: {
          title: 'Operator ' + (operatorI + 3),
          inputs: {
            input_1: {
              label: 'Input 1',
            }
          },
          outputs: {
            output_1: {
              label: 'Output 1',
            }
          }
        }
      }; 

      operatorI++;

      $('#example').flowchart('createOperator', operatorId, operatorData);
    });

    $('#delete_selected_button').click(function() {
      $('#example').flowchart('deleteSelected');
    });
   }	
})