({
	initInfo : function(component, event, helper) {
		var main = component.get('c.getTasksInfo');
        main.setCallback(this, function(response){
            var state = response.getState();
            var selectedTASK = "first";
            if(document.URL.indexOf('task=') > 0){
                selectedTASK = document.URL.substring(document.URL.indexOf('task=')).substring(5);
            }
            
            if(component.isValid() && state === "SUCCESS"){
                var tasks = JSON.parse(response.getReturnValue());
                $(".litabs").each(function( index, element ) {
                    $(element).html("");
                });    
                $(".response").each(function( index, element ) {
                    $(element).html("");
                }); 
                $(tasks.tasksDescription).each(function( index ) {
                    var aux = index +1;
                    if (tasks.tasksDescription[index].name==selectedTASK || (selectedTASK == "first" && index==0)){
                        $(".litabs").each(function( indexlitabs, element ) {
                            $(element).append('<li id="litab'+aux+'" class="slds-nav-vertical__item slds-is-active"><a href="#/n/EVR__EVR_everCI_Task?task='+tasks.tasksDescription[index].name+'" class="slds-nav-vertical__action" aria-describedby="entity-header" aria-current="page" >'+tasks.tasksDescription[index].name+'</a></li>');
                        });
                        $(".response").each(function( indexlitabs, element ) {
                            $(element).append(tasks.tasksDescription[index].description);
                        });
                    }else{
                        $(".litabs").each(function( indexlitabs, element ) {
                    		$(element).append('<li id="litab'+aux+'" class="slds-nav-vertical__item"><a href="#/n/EVR__EVR_everCI_Task?task='+tasks.tasksDescription[index].name+'" class="slds-nav-vertical__action" aria-describedby="entity-header">'+tasks.tasksDescription[index].name+'</a></li>');    
                        }); 
                    }
                  	
                });

            }else{
                alert('There was a problem invoking everci-Cloud');
            }
        });
        $A.enqueueAction(main);
	}
})