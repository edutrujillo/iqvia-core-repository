({
	callSSL : function(component, event, helper) {
		var main = component.get('c.getInfo');
        main.setCallback(this, function(response){
            var state = response.getState();
			alert(response.getReturnValue());
        });
        $A.enqueueAction(main);
	},
    callGoogleInfo : function(component, event, helper) {
		var main = component.get('c.getGoogleInfo');
        main.setCallback(this, function(response){
            var state = response.getState();
			alert(response.getReturnValue());
        });
        $A.enqueueAction(main);
	}
    
})