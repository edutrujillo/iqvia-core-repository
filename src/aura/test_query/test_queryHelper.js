({
	callQuery : function(component, id) {
		var main = component.get('c.Hola');
        main.setParams({recorid : id});
        main.setCallback(this, function(response){
            var state = response.getState();
			alert(response.getReturnValue());
        });
        $A.enqueueAction(main);
	}
})